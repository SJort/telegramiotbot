# IOT Telegram bot  
Running on Generic ESP8266 Module  
Uses the CTBot libary: https://github.com/shurillu/CTBot  
This library depends on another library: ArduinoJson version 5.13.5: https://github.com/bblanchon/ArduinoJson  

# Setup Arduino Editor
* Install above libraries, pay attention to the needed version.
* Put URL found on https://arduino.esp8266.com/stable/package_esp8266com_index.json at Preferences > Additional Board Manager URLs.
* Install 'esp8266' in boards manager.  
* Select 'Generic ESP8266 Module' as board.
* Set upload speed to 921600.
* Select connected ESP8266 board.

# Setup ESP8266 board
Connect ground, 3v3 and pin 2 to the relay
