#define relayPin 7
#define ledPower 13

void setup() {
  Serial.begin(9600);
  Serial.println("Started FeedBackLoop");
  pinMode(relayPin, OUTPUT);
  
  pinMode(ledPower, OUTPUT);
  digitalWrite(ledPower, HIGH);
}

void loop() {
  digitalWrite(relayPin, HIGH);
  delay(200);
  digitalWrite(relayPin, LOW);
  delay(200);
}

