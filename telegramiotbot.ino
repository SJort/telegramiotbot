/**
   For Generic ESP8266 Module
   Uses the CTBot libary: https://github.com/shurillu/CTBot
   This library depends on another library: ArduinoJson version 5.13.5: https://github.com/bblanchon/ArduinoJson
*/

#define relayPin 4

#include "CTBot.h"
CTBot myBot;

String ssid  = "JortSpot";
String pass  = "SpotJort";
String token = "778480439:AAFbbMZrarDagUHFPfBFPdrJqj7xEoU6p54";

TBMessage msg;

void setup() {
  pinMode(relayPin, OUTPUT);
  disableRelay();
  myBot.wifiConnect(ssid, pass);
  myBot.setTelegramToken(token);
}

void loop() {
  if (myBot.getNewMessage(msg)) {
    if(msg.messageType != CTBotMessageText){
      sendMessage("Not a message");
      return;
    }
    
    if (isCommand("/press")) {
      hold(200);
    }
    else if (isCommand("/hold")) {
      int parameter = getParameter();
      if(parameter == 0){
        parameter = 5000;
      }
      hold(parameter);
    }
    else if (isCommand("/ping")) {
      sendMessage("Im here!");
    }
  }
}

void hold(int ms){\
  enableRelay();
  delay(ms);
  disableRelay(); 
  String message = String("Held button for ");
  message.concat(ms);
  message.concat("ms.");
  sendMessage(message);
}

int getParameter() {
  String message = msg.text;
  int index = message.indexOf(' ');
  if(index == -1){
    return -1;
  }
  if(index == message.length()){
    return -1;
  }

  String parameter = message.substring(index);
  return parameter.toInt();
  
}

bool isCommand(String command) {
  String message = msg.text;
  //telegram commands are lowercase
  message.toLowerCase();
  return message.startsWith(command);
}

void sendMessage(String text) {
  myBot.sendMessage(msg.sender.id, text);
}

void enableRelay() {
  digitalWrite(relayPin, HIGH);
}

void disableRelay() {
  digitalWrite(relayPin, LOW);
}
